package com.hotelreservation.sb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelReservationSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelReservationSpringBootApplication.class, args);
	}

}
